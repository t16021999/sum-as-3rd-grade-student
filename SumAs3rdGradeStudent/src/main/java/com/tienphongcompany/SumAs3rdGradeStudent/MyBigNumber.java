package com.tienphongcompany.SumAs3rdGradeStudent;




public class MyBigNumber {

	public String sum(String stn1, String stn2) {
		String result = "";
		int n1 = stn1.length();
		int n2 = stn2.length();
		// Đảm bảo chuỗi thứ 2 luôn lớn hơn chuỗi thứ 1
		if (n1 > n2) {
			String swap = stn1;
			stn1 = stn2;
			stn2 = swap;
		}
		// quay đầu chuỗi
		stn1 = new StringBuffer(stn1).reverse().toString();
		stn2 = new StringBuffer(stn2).reverse().toString();
		// khởi tạo số dư
		int surplus = 0;
		// tính tổng các số cuối ở chuỗi ngắn hơn
		for (int i = 0; i < n1; i++) {
			int sumLast = Integer.parseInt(stn1.charAt(i) + "") + Integer.parseInt(stn2.charAt(i) + "") + surplus;
			result += String.valueOf(sumLast % 10);
			surplus = sumLast / 10;
		}
		// cộng phần còn lại của chuỗi dài hơn
		for (int i = n1; i < n2; i++) {
			int sumLast = Integer.parseInt(stn2.charAt(i) - '0' + "") + surplus;
			result += String.valueOf(sumLast % 10);
			surplus = sumLast / 10;
		}
		// trường hợp cuối cùng đã cộng hết 2 dãy số mà vẫn còn số dư
		if (surplus > 0) {
			result += String.valueOf(surplus);
		}
		result = new StringBuffer(result).reverse().toString();
		System.out.println(result);
		return result;
	}

}
