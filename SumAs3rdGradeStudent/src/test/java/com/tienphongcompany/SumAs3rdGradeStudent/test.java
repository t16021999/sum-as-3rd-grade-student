package com.tienphongcompany.SumAs3rdGradeStudent;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

public class test {
	MyBigNumber number=new MyBigNumber();
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSum1() {
		
		
		assertEquals(number.sum("1999", "19999"),"21998");
	}
	
	@Test
	public void testSum2() {
		MyBigNumber number=new MyBigNumber();
		
		assertEquals(number.sum("1", "19999"),"20000");
	}
	@Test
	public void testSum3() {
		MyBigNumber number=new MyBigNumber();
		
		assertEquals(number.sum("1", "99999"),"100000");
	}

}
